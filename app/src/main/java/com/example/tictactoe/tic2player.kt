package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.example.tictactoe.Board.Companion.PLAYER
import kotlinx.android.synthetic.main.activity_intro.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.toolbar

class tic2player : AppCompatActivity() {
    //Skrip ini untuk menjalankan mode 2 player
    private val boardCells = Array(3) { arrayOfNulls<ImageView>(3) }
    //creating the board instance
    var board = Board()
    var currentPlayer = Board.PLAYER

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            title = "TicTacToe"
        }
        //calling the function to load our tic tac toe board
        loadBoard()
        //restart functionality
        button_restart.setOnClickListener {
            //creating a new board instance
            //it will empty every cell
            board = Board()

            //setting the result to empty
            text_view_result.text = ""

            //this function will map the internal board
            //to the visual board
            mapBoardToUi()
        }
        button_res.setOnClickListener{
            p1.text = "0"
            p2.text = "0"
            d1.text = "0"
        }
    }
    //function is mapping
    //the internal board to the ImageView array board
    private fun mapBoardToUi() {
        for (i in board.board.indices) {
            for (j in board.board.indices) {
                when (board.board[i][j]) {
                    Board.PLAYER -> {
                        boardCells[i][j]?.setImageResource(R.drawable.circle)
                        boardCells[i][j]?.isEnabled = false
                    }
                    Board.COMPUTER -> {
                        boardCells[i][j]?.setImageResource(R.drawable.cross)
                        boardCells[i][j]?.isEnabled = false
                    }
                    else -> {
                        boardCells[i][j]?.setImageResource(0)
                        boardCells[i][j]?.isEnabled = true
                    }
                }
            }
        }
    }
    private fun player2win(){
        var a = p2.text.toString().toInt()
        text_view_result.text = "PLayer 2 Won"
        a = a + 1
        p2.text = a.toString()
    }
    private fun player1win(){
        var a = p1.text.toString().toInt()
        text_view_result.text = "Player 1 Won"
        a= a + 1
        p1.text = a.toString()
    }
    private fun draw(){
        var a = d1.text.toString().toInt()
        text_view_result.text = "Game Tied"
        a = a + 1
        d1.text = a.toString()
    }
    private fun loadBoard() {

        for (i in boardCells.indices) {
            for (j in boardCells.indices) {
                boardCells[i][j] = ImageView(this)
                boardCells[i][j]?.layoutParams = GridLayout.LayoutParams().apply {
                    rowSpec = GridLayout.spec(i)
                    columnSpec = GridLayout.spec(j)
                    width = 320
                    height = 270
                    bottomMargin = 5
                    topMargin = 1
                    leftMargin = 5
                    rightMargin = 5
                }
                boardCells[i][j]?.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))


//attached a click listener to the board
                boardCells[i][j]?.setOnClickListener(CellClickListener(i, j))
                layout_board.addView(boardCells[i][j])
            }
        }
    }
    inner class CellClickListener(
        val i: Int,
        val j: Int
    ) : View.OnClickListener {

        override fun onClick(p0: View?) {

            //checking if the game is not over
            if (!board.isGameOver) {

                //creating a new cell with the clicked index
                if(currentPlayer == Board.PLAYER) {
                    val cell = Cell(i, j)
                    board.placeMove(cell, Board.PLAYER)
                    currentPlayer = Board.PLAYER2
                }else {
                    val cell = Cell(i, j)
//                performing the move for computer
                    board.placeMove(cell, Board.PLAYER2)
                    currentPlayer = Board.PLAYER
                }



                //mapping the internal board to visual board
                mapBoardToUi()
                when {
                    board.hasPlayer2Won() -> player2win()
                    board.hasPlayerWon() -> player1win()
                    board.isGameOver -> draw()
                }
            }
            //Displaying the results
            //according to the game status

        }
    }
}
