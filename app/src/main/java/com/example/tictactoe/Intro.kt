package com.example.tictactoe

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_intro.*

class Intro : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            title = "TicTacToe"
        }
    }

    fun onClickAI(view: View) {
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    fun onClick2p(view: View) {
        val intent = Intent(this,tic2player::class.java)
        startActivity(intent)
    }
}
